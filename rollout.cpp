#include "rollout.h"

Rollout::Rollout()
    : Board()
{

}

Rollout::Rollout(const Board &boardToCopy)
    : Board(boardToCopy)
{

}

Rollout::~Rollout()
{

}

unsigned char Rollout::rollout()
{
    std::random_device rseed;
    std::mt19937 rgen(std::chrono::system_clock::now().time_since_epoch().count());


    unsigned char winner = 0;
    std::unique_ptr<std::vector<std::pair<unsigned int, unsigned int>>> moves = getAvailableMoves();
    std::uniform_int_distribution<unsigned int> idist;


    while((winner == 0) && (moves->size() != 0))
    {
        // Select and play a random move
        idist = std::uniform_int_distribution<unsigned int>(0,moves->size()-1);
        winner = playMove((*moves)[idist(rgen)]);

        moves = getAvailableMoves();
    }
    return winner;
}
