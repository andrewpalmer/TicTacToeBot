#ifndef BOARD_H
#define BOARD_H

#include <utility> // For std::pair
#include <iostream>
#include <memory>
#include <vector>

class Board
{
public:
    Board();
    Board(const Board &toCopyFrom); // Copy constructor

    void copyFromBoard(const Board &toCopyFrom);
    void copyFromBoard(const Board *toCopyFrom);

    virtual ~Board();

    void printBoard();
    unsigned char playMove(const std::pair<unsigned int, unsigned int> &move);
    unsigned char checkMacroSquareForWinner(const unsigned int &macro_col, const unsigned int &macro_row);
    unsigned char checkMacroForWinner();
    std::unique_ptr<std::vector<std::pair<unsigned int, unsigned int>>> getAvailableMoves();

    unsigned int getCurrentPlayer();

private:

    // Data
    // Board positions are referenced by col row

    unsigned char macro_valid_play[3][3]; // Valid macro squares that can be played in
    unsigned char macro_player_winner[3][3]; // 1 for player 1, 2 for player 2
    unsigned char moves_remaining[3][3];
    unsigned char board[9][9]; // 1 for player 1, 2 for player 2
    unsigned char current_player;
};

#endif // BOARD_H
