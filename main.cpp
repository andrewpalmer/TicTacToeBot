#include <iostream>
#include "board.h"

#include <chrono>
#include <thread>

#include "rollout.h"
#include "communicator.h"
#include "node.h"
#include "strategy.h"
#include "mcts.h"

int main()
{
    Communicator comms = Communicator();
    std::shared_ptr<MCTS> mcts_object(new MCTS());
    comms.setObserver(static_cast<std::shared_ptr<Strategy>> (mcts_object));


    std::thread t(&Communicator::dealWithInputs, &comms);

    // Run MCTS
    mcts_object->run();

    t.join();




//    MCTS a;
//    unsigned int p = 1;
//    a.playerID(p);

//    a.searchFor(1000);
//    a.best();

//    std::pair<unsigned int, unsigned int> move(1,1);
//    a.sendMove(move);



//    for (int i = 0; i < 1000000; i++)
//    {
//        if (i%10000 == 0)
//        {
//            std::cerr << i << std::endl;
//        }
//        a.oneSearch();
//    }

//    a.best();







//    Board b = Board();
//    std::pair<unsigned int, unsigned int> move(1,1);
//    Node n(b, move);

//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();






////    n.spawnChildren();

//    move = std::pair<unsigned int, unsigned int>(3,5);
//    auto new_node = n.getChildNode(move);
//    new_node->updateScore(Node::winScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(3,3);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::drawScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(3,4);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::drawScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(4,3);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::loseScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(4,4);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::loseScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(4,5);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::loseScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(5,3);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::drawScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(5,4);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::loseScore);
//    new_node->updateNumVisits();

//    move = std::pair<unsigned int, unsigned int>(5,5);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::drawScore);
//    new_node->updateNumVisits();

//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();
//    n.updateNumVisits();

//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();
//    n.updateNumVisitsAfter();

//    move = std::pair<unsigned int, unsigned int>(3,3);
//    new_node = n.getChildNode(move);
//    new_node->updateScore(Node::winScore);
//    new_node->updateNumVisits();

//    n.updateNumVisits();
//    n.updateNumVisitsAfter();

//    move = n.getBestMoveToPlay();
//    n.getBestNodeUCT();








//    Board b = Board();
//    Rollout r;


//    b.playMove(std::pair<unsigned int, unsigned int>(2,0));
//    b.printBoard();

//    auto moves = b.getAvailableMoves();
//    for(int i=0; i < moves->size(); i++)
//    {
//        std::cerr << (*moves)[i].first << " " << (*moves)[i].second << std::endl;
//    }

//    b.playMove(std::pair<unsigned int, unsigned int>(2,0));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(3,3));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(1,1));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(3,4));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(0,2));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(3,5));
//    b.printBoard();


//    b.playMove(std::pair<unsigned int, unsigned int>(5,0));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,3));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(4,1));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,4));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(3,2));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,5));
//    b.printBoard();


//    b.playMove(std::pair<unsigned int, unsigned int>(8,0));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,6));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(7,1));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,7));
//    b.printBoard();

//    b.playMove(std::pair<unsigned int, unsigned int>(6,2));
//    b.printBoard();



//    std::random_device rseed;
//    std::mt19937 rgen(std::chrono::system_clock::now().time_since_epoch().count());
//    std::uniform_int_distribution<unsigned int> idist(0,100);
//    for(int i = 0; i < 10; i++)
//    {
//        std::cout << idist(rgen) << std::endl;
//    }

//    std::cout << std::chrono::system_clock::now().time_since_epoch().count() << std::endl;



    return 0;
}

