#ifndef NODE_H
#define NODE_H

#include <vector>
#include <memory>
#include <utility>

#include <cmath>

#include "board.h"
#include "rollout.h"

class Node
{
public:
    // Scores for a win and draw
    static const unsigned int winScore;
    static const unsigned int drawScore;
    static const unsigned int loseScore;

    Node();
    Node(Board &state, std::pair<unsigned int, unsigned int> &move);
    ~Node();

    void updateNumVisits();
    void updateNumVisitsAfter();
    void updateScore(unsigned int val);

    unsigned long getNumVisits();
    unsigned long getScore();

    bool checkLeafNode();
    bool checkTerminalNode();
    unsigned char checkGuaranteedWinner();

    Board * getCurrentState();

    // Populate the children vector
    void spawnChildren();

    // Function for getting one of the children nodes
    std::shared_ptr<Node> getChildNode(std::pair<unsigned int, unsigned int> &move);

    // Get best move node as calculated using UCT
    std::shared_ptr<Node> getBestNodeUCT(unsigned int &playerID);

    // Get the best move to play
    std::pair<unsigned int, unsigned int> getBestMoveToPlay();


private:
    Board currentState;

    std::vector<std::shared_ptr<Node>> children;
    std::unique_ptr<std::vector<std::pair<unsigned int, unsigned int>>> moves;

    unsigned long numVisits;
    unsigned long numVisitsAfter; // Number of visits to children
    unsigned long score;
    bool leafNode;
    unsigned char guaranteedWinner;
    bool terminalNode;

    // Threshold for number of visits before spawning children
    static const unsigned int visitLim;
};

#endif // NODE_H
