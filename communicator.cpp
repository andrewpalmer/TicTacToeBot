#include "communicator.h"

Communicator::Communicator()
    : player(0), timebank(10000), timebank_max(10000), time_per_move(500)
{
    for (int i = 0; i < 81; i++)
    {
        board[i] = 0;
    }
}

Communicator::~Communicator()
{

}

void Communicator::setObserver(std::shared_ptr<Strategy> observing)
{
    observing_object = observing;
}

void Communicator::dealWithInputs()
{
    std::string line;

    while(std::getline(std::cin, line))
    {
        std::vector<std::string> tokens = split(line, ' ');
        if(tokens[0] == "exit")
        {
            observing_object->exit();
            break;
        }
        else
        {
            if(tokens[0] == "settings")
            {
                if(tokens[1] == "your_botid")
                {
                    player = std::stoi(tokens[2]);
                    observing_object->playerID(player);

                    std::cerr << "Bot ID updated to " << player << std::endl;
                }
                else if (tokens[1] == "timebank")
                {
                    timebank_max = std::stoi(tokens[2]);
                    observing_object->timebankMax(timebank_max);

                    std::cerr << "Timebank updated to " << timebank_max << std::endl;
                }
                else if (tokens[1] == "time_per_move")
                {
                    time_per_move = std::stoi(tokens[2]);
                    observing_object->timePerTurn(time_per_move);

                    std::cerr << "Time per move updated to " << time_per_move << std::endl;
                }
            }
            else if (tokens[0] == "update")
            {
                if (tokens[1] == "game" && tokens[2] == "field")
                {
                    std::vector<std::string> fields = split(tokens[3], ',');
                    unsigned int num;

                    for(int i = 0; i<81; i++)
                    {
                        num = std::stoi(fields[i]);
                        if(num != board[i] && num != player)
                        {
                            // Then this was the move played by the opponent
                            std::cerr << "Move " << i << " " << i%9 << " " << i/9 << std::endl;

                            // Notify the observer
                            observing_object->updateState(std::pair<unsigned int, unsigned int>(i%9,i/9));
                        }
                        std::cerr << std::endl;
                        std::cerr << board[i] << " " << num << std::endl;
                        board[i] = num;

                    }
                }
            }
            else if (tokens[0] == "action")
            {
                if (tokens[1] == "move")
                {
                    // Update the timebank and tell the observing object to come up with a move
                    timebank = std::stoi(tokens[2]);
                    observing_object->playMove(timebank);
                }
            }
        }
    }
}


std::vector<std::string> Communicator::split(std::string& input, char delimiter)
{
  std::vector<std::string> tokens;
  std::stringstream ss(input);
  std::string token;

    while (getline(ss, token, delimiter))
    {
        tokens.push_back(token);
    }

  return tokens;
}

unsigned int Communicator::getTimebank()
{
    return timebank;
}

unsigned int Communicator::getMaxTimebank()
{
    return timebank_max;
}

unsigned int Communicator::getTimePerMove()
{
    return time_per_move;
}

unsigned int Communicator::playerID()
{
    return player;
}
