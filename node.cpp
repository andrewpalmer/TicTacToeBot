#include "node.h"

const unsigned int Node::winScore = 5;
const unsigned int Node::drawScore = 1;
const unsigned int Node::loseScore = 0;

const unsigned int Node::visitLim = 10;

Node::Node()
    : numVisits(0), numVisitsAfter(0), score(0), leafNode(false), guaranteedWinner(0), terminalNode(false)
{
    moves = currentState.getAvailableMoves();
    // It's probably the start of the game, so we'll populate the children as well
    spawnChildren();
}

Node::Node(Board &state, std::pair<unsigned int, unsigned int> &move)
    : currentState(state), numVisits(0), numVisitsAfter(0), score(0), leafNode(true), guaranteedWinner(0), terminalNode(false)
{
    // Check whether this returns a winner or not
    // If it returns a winner, then mark as a terminal node and update guaranteedWinner
    unsigned char winner = currentState.playMove(move);

    if (winner != 0)
    {
        terminalNode = true;
        guaranteedWinner = winner;
    }
    else
    {
        // Get the available moves and store them in moves
        // If the length of available moves is 0, then mark it as a terminal node
        moves = currentState.getAvailableMoves();
        if (moves->size() == 0)
        {
            terminalNode = true;
        }
    }
}

Node::~Node()
{

}

void Node::updateNumVisits()
{
    numVisits++;
    if ((numVisits == visitLim) && leafNode && !terminalNode)
    {
        spawnChildren();
    }
}

void Node::updateNumVisitsAfter()
{
    numVisitsAfter++;
}

void Node::updateScore(unsigned int val)
{
    score += val;
//    std::cerr << "Updated score to " << score << std::endl;
}

unsigned long Node::getNumVisits()
{
    return numVisits;
}

unsigned long Node::getScore()
{
    return score;
}

bool Node::checkLeafNode()
{
    return leafNode;
}

bool Node::checkTerminalNode()
{
    return terminalNode;
}

unsigned char Node::checkGuaranteedWinner()
{
    return guaranteedWinner;
}

Board * Node::getCurrentState()
{
    return &currentState;
}

void Node::spawnChildren()
{
//    std::cerr << "Spawning children" << std::endl;

    for(unsigned int i=0; i < moves->size(); i++)
    {
        children.push_back(std::shared_ptr<Node>(new Node(currentState,(*moves)[i])));
    }
    leafNode = false;

//    std::cerr << "Finished spawning children" << std::endl;
}

std::shared_ptr<Node> Node::getChildNode(std::pair<unsigned int, unsigned int> &move)
{
    std::cerr << "Getting child node" << std::endl;

    if(children.size() == 0)
    {
        // If the children haven't been spawned yet, then spawn them
        spawnChildren();
    }

    for(unsigned int i=0; i < moves->size(); i++)
    {
        std::cerr << i << " " << move.first << " " << move.second << " " << (*moves)[i].first << " " << (*moves)[i].second << std::endl;
        if(((*moves)[i].first == move.first) && ((*moves)[i].second == move.second))
        {
            return children[i];
        }
    }
    return nullptr; // error
}

std::shared_ptr<Node> Node::getBestNodeUCT(unsigned int &playerID)
{
    if(moves->size() != children.size())
    {
        std::cerr << "ERROR: Move and children vectors are different sizes" << std::endl;
        return nullptr;
    }

    // If a move hasn't been visited, then return that
    // Otherwise use the UCT formula to get the best move
    unsigned int i;
    unsigned int best_move = 0;

    double best_score = 0;
    double tmpscore = 0;

    for (i = 0; i < moves->size(); i++)
    {
        if (children[i]->getNumVisits() == 0)
        {
//            std::cerr << "Returning index " << i << " Move: " << (*moves)[i].first << " " << (*moves)[i].second << " as it hasn't been visited" << std::endl;
            numVisitsAfter++;
            return children[i];
        }
        else
        {
            unsigned int childScore;
            if (playerID == currentState.getCurrentPlayer())
            {
                // If the move is to be played by our bot
                childScore = children[i]->getScore();
            }
            else
            {
                // Otherwise it is the move to be played by the opponent
                childScore = winScore * children[i]->getNumVisits() - children[i]->getScore();
            }


            tmpscore = static_cast<double>(childScore)/static_cast<double>(winScore * children[i]->getNumVisits()) + 1.414214 * std::sqrt(std::log(static_cast<double>(numVisitsAfter))/static_cast<double>(children[i]->getNumVisits()));

//            std::cerr << i << " " << (*moves)[i].first << " " << (*moves)[i].second << " Visits: " << children[i]->getNumVisits() << " Score: " << children[i]->getScore() << " UCT: " << tmpscore << std::endl;

            if (tmpscore > best_score)
            {
                best_score = tmpscore;
                best_move = i;
            }
        }
    }

//    std::cerr << "Best move to play is index " << best_move << " Move: " << (*moves)[best_move].first << " " << (*moves)[best_move].second << " with score " << best_score << std::endl;
    numVisitsAfter++;

    return children[best_move];
}

std::pair<unsigned int, unsigned int> Node::getBestMoveToPlay()
{
    std::cerr << "Getting best move to play" << std::endl;

    if(moves->size() != children.size())
    {
        std::cerr << "ERROR: Move and children vectors are different sizes" << std::endl;
        return std::pair<unsigned int, unsigned int>(-1,-1);
    }

    unsigned int i;
    unsigned int best_move = 0;


    unsigned long best_score = 0;
    unsigned long tmpscore = 0;

    // Uncomment this if dividing by number of visits, and comment out above 2 lines
//    float best_score = 0;
//    float score = 0;


    // Some people suggest just using score
    // Others suggest dividing score by number of visits
    for (i = 0; i < moves->size(); i++)
    {
        tmpscore = children[i]->getScore();

        // Uncomment this if dividing by number of visits, and comment out above line
//        if (children[i]->getNumVisits() == 0){
//            tmpscore = 0;
//        }
//        else{
//            tmpscore = static_cast<float>(children[i]->getScore())/static_cast<float>(children[i]->getNumVisits());
//        }

        std::cerr << i << " " << (*moves)[i].first << " " << (*moves)[i].second << " " << tmpscore << std::endl;

        if(tmpscore > best_score)
        {
            best_score = tmpscore;
            best_move = i;
        }
    }

    std::cerr << "Best move to play is index " << best_move << " Move: " << (*moves)[best_move].first << " " << (*moves)[best_move].second << " with score " << best_score << std::endl;

    return (*moves)[best_move];
}
