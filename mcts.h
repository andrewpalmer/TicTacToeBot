#ifndef MCTS_H
#define MCTS_H

#include <vector>
#include <memory>
#include <utility>
#include <chrono>

#include "strategy.h"
#include "node.h"
#include "communicator.h"
#include "rollout.h"
#include "board.h"

class MCTS : public Strategy
{
public:
    MCTS();

    void playMove(const unsigned int &timebank);
    void updateState(std::pair<unsigned int, unsigned int> move);
    void playerID(unsigned int &id);
    void timebankMax(unsigned int &t);
    void timePerTurn(unsigned int &t);
    void exit();

    void oneSearch(); // Performs one episode of MCTS search
    void searchFor(unsigned int t); // Searches for a duration

    void sendMove(std::pair<unsigned int, unsigned int> &move);

    void run();


    void best();

private:

    unsigned int calcSearchDuration();

    std::shared_ptr<Node> root;
    unsigned int player_ID;
    unsigned int timebank_max;
    unsigned int time_per_turn;

    bool move_flag;
    unsigned int timebank_remaining;
    bool exit_flag;

    bool opponent_move_flag;
    std::pair<unsigned int, unsigned int> opponent_move;

    std::vector<std::shared_ptr<Node>> parents;

    Rollout rollout;

};

#endif // MCTS_H
