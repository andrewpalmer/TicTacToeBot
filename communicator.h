#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <memory>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <atomic>

#include "strategy.h"

class Communicator
{
public:
    Communicator();
    ~Communicator();


    void setObserver(std::shared_ptr<Strategy> observing);

    void dealWithInputs();

    unsigned int getTimebank();
    unsigned int getMaxTimebank();
    unsigned int getTimePerMove();
    unsigned int playerID();

private:
    std::shared_ptr<Strategy> observing_object;

    unsigned int player; // Player number of the bot
    unsigned int board[81]; // The board as last received by the communicator
    unsigned int timebank;
    unsigned int timebank_max;
    unsigned int time_per_move;

    std::vector<std::string> split(std::string& input, char delimiter);

};

#endif // COMMUNICATOR_H
