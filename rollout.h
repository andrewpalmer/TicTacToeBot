#ifndef ROLLOUT_H
#define ROLLOUT_H

#include <random>
#include <chrono>

#include "board.h"

class Rollout : public Board
{
public:
    Rollout();
    Rollout(const Board &boardToCopy);
    ~Rollout();

    unsigned char rollout();

};

#endif // ROLLOUT_H
