#include "board.h"

Board::Board()
    : current_player(1)
{
//    std::cerr << "Constructing default board" << std::endl;
    unsigned int col, row;
    for(col=0; col<3; col++)
    {
        for(row=0; row<3; row++)
        {
            macro_valid_play[col][row] = 1;
            macro_player_winner[col][row] = 0;
            moves_remaining[col][row] = 9;
        }
    }

    for(col=0; col<9; col++)
    {
        for(row=0; row<9; row++)
        {
            board[col][row] = 0;
        }
    }
}

Board::Board(const Board &toCopyFrom)
    : current_player(toCopyFrom.current_player)
{
    copyFromBoard(toCopyFrom);
}

void Board::copyFromBoard(const Board &toCopyFrom)
{
    unsigned int col, row;
    for(col=0; col<3; col++)
    {
        for(row=0; row<3; row++)
        {
            macro_valid_play[col][row] = toCopyFrom.macro_valid_play[col][row];
            macro_player_winner[col][row] = toCopyFrom.macro_player_winner[col][row];
            moves_remaining[col][row] = toCopyFrom.moves_remaining[col][row];
        }
    }
    for(col=0; col<9; col++)
    {
        for(row=0; row<9; row++)
        {
            board[col][row] = toCopyFrom.board[col][row];
        }
    }
}

void Board::copyFromBoard(const Board *toCopyFrom)
{
    unsigned int col, row;
    for(col=0; col<3; col++)
    {
        for(row=0; row<3; row++)
        {
            macro_valid_play[col][row] = toCopyFrom->macro_valid_play[col][row];
            macro_player_winner[col][row] = toCopyFrom->macro_player_winner[col][row];
            moves_remaining[col][row] = toCopyFrom->moves_remaining[col][row];
        }
    }
    for(col=0; col<9; col++)
    {
        for(row=0; row<9; row++)
        {
            board[col][row] = toCopyFrom->board[col][row];
        }
    }
}

Board::~Board()
{

}

/* Prints out the current state of the board to cerr */
void Board::printBoard()
{
    std::cerr << std::endl << "Current state of the board. Current player is player " << static_cast<unsigned int>(current_player) << std::endl << std::endl;
    unsigned int col, row;

    std::cerr << "Board:" << std::endl;
    for(row=0; row<9; row++)
    {
        for(col=0; col<9; col++)
        {
            std::cerr << static_cast<unsigned int>(board[col][row]) << " ";
            if(col%3 == 2) {
                std::cerr << " ";
            }
        }
        std::cerr << std::endl;
        if(row%3 == 2) {
            std::cerr << std::endl;
        }
    }

    std::cerr << std::endl;
    std::cerr << "Macro board, valid play squares, and moves remaining:" << std::endl;
    for(row=0; row<3; row++)
    {
        for(col=0; col<3; col++)
        {
            std::cerr << static_cast<unsigned int>(macro_player_winner[col][row]) << " ";
        }

        std::cerr << "  ";

        for(col=0; col<3; col++)
        {
            std::cerr << static_cast<unsigned int>(macro_valid_play[col][row]) << " ";
        }

        std::cerr << "  ";

        for(col=0; col<3; col++)
        {
            std::cerr << static_cast<unsigned int>(moves_remaining[col][row]) << " ";
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
}

unsigned char Board::checkMacroSquareForWinner(const unsigned int &macro_col, const unsigned int &macro_row)
{
    unsigned int offset_col = macro_col * 3;
    unsigned int offset_row = macro_row * 3;

    // First check all of the options with the middle square
    unsigned char candidate = board[offset_col+1][offset_row+1];
    if(candidate == current_player && (
            ((board[offset_col][offset_row] == candidate) && (board[offset_col+2][offset_row+2] == candidate)) || // Diagonal
            ((board[offset_col][offset_row+2] == candidate) && (board[offset_col+2][offset_row] == candidate)) || // Diagonal
            ((board[offset_col+1][offset_row] == candidate) && (board[offset_col+1][offset_row+2] == candidate)) || // Vertical
            ((board[offset_col][offset_row+1] == candidate) && (board[offset_col+2][offset_row+1] == candidate)) // Horizontal
                ))
    {
        return candidate;
    }

    // Now check top left corner
    candidate = board[offset_col][offset_row];
    if(candidate == current_player && (
                ((board[offset_col][offset_row+1] == candidate) && (board[offset_col][offset_row+2] == candidate)) ||
                ((board[offset_col+1][offset_row] == candidate) && (board[offset_col+2][offset_row] == candidate))
                ))
    {
        return candidate;
    }

    // Finally check the bottom right corner
    candidate = board[offset_col+2][offset_row+2];
    if(candidate == current_player && (
                ((board[offset_col+2][offset_row] == candidate) && (board[offset_col+2][offset_row+1] == candidate)) ||
                ((board[offset_col][offset_row+2] == candidate) && (board[offset_col+1][offset_row+2] == candidate))
                ))
    {
        return candidate;
    }

    return 0;
}

unsigned char Board::checkMacroForWinner()
{
    // First check all of the options with the middle square
    unsigned char candidate = macro_player_winner[1][1];
    if(candidate == current_player && (
            ((macro_player_winner[0][0] == candidate) && (macro_player_winner[2][2] == candidate)) || // Diagonal
            ((macro_player_winner[0][2] == candidate) && (macro_player_winner[2][0] == candidate)) || // Diagonal
            ((macro_player_winner[1][0] == candidate) && (macro_player_winner[1][2] == candidate)) || // Vertical
            ((macro_player_winner[0][1] == candidate) && (macro_player_winner[2][1] == candidate)) // Horizontal
                ))
    {
        return candidate;
    }

    // Now check top left corner
    candidate = macro_player_winner[0][0];
    if(candidate == current_player && (
                ((macro_player_winner[0][1] == candidate) && (macro_player_winner[0][2] == candidate)) ||
                ((macro_player_winner[1][0] == candidate) && (macro_player_winner[2][0] == candidate))
                ))
    {
        return candidate;
    }

    // Finally check the bottom right corner
    candidate = macro_player_winner[2][2];
    if(candidate == current_player && (
                ((macro_player_winner[2][0] == candidate) && (macro_player_winner[2][1] == candidate)) ||
                ((macro_player_winner[0][2] == candidate) && (macro_player_winner[1][2] == candidate))
                ))
    {
        return candidate;
    }

    return 0;
}

unsigned char Board::playMove(const std::pair<unsigned int, unsigned int> &move)
{
    // First update the board with the move
    board[move.first][move.second] = current_player;

    unsigned int macro_col = move.first/3;
    unsigned int macro_row = move.second/3;

    moves_remaining[macro_col][macro_row]--;

    // Check whether it resulted in a winner in that macro square
    unsigned char winner = checkMacroSquareForWinner(macro_col, macro_row);

    // If the macro square had a winner, check for an overall winner
    if(winner != 0)
    {
        macro_player_winner[macro_col][macro_row] = winner;
        moves_remaining[macro_col][macro_row] = 0;
        winner = checkMacroForWinner();
    }

    if(winner != 0)
    {
        return winner;
    }

    // Update the macro squares that can be played in
    macro_col = move.first - 3 * macro_col;
    macro_row = move.second - 3 * macro_row;

    unsigned int col, row;

    if((macro_player_winner[macro_col][macro_row] == 0) && (moves_remaining[macro_col][macro_row] != 0))
    {
        for(col=0; col<3; col++)
        {
            for(row=0; row<3; row++)
            {
                macro_valid_play[col][row] = 0;
            }
        }
        macro_valid_play[macro_col][macro_row] = 1;
    }
    else
    {
        for(col=0; col<3; col++)
        {
            for(row=0; row<3; row++)
            {
                if(moves_remaining[col][row] != 0)
                {
                    macro_valid_play[col][row] = 1;
                }
                else
                {
                    macro_valid_play[col][row] = 0;
                }
            }
        }
    }

    current_player = current_player%2 + 1;

    return 0;
}

std::unique_ptr<std::vector<std::pair<unsigned int, unsigned int>>> Board::getAvailableMoves()
{
    std::unique_ptr<std::vector<std::pair<unsigned int, unsigned int>>> moves (new std::vector<std::pair<unsigned int, unsigned int>>());

    unsigned int macro_col, macro_row, offset_col, offset_row, col, row;
    for(macro_col=0; macro_col<3; macro_col++)
    {
        for(macro_row=0; macro_row<3; macro_row++)
        {
            if(macro_valid_play[macro_col][macro_row] != 0)
            {
                offset_col = macro_col*3;
                offset_row = macro_row*3;
                for(col=offset_col; col<offset_col+3; col++)
                {
                    for(row=offset_row; row<offset_row+3; row++)
                    {
                        if(board[col][row] == 0)
                        {
                            moves->push_back(std::pair<unsigned int, unsigned int>(col,row));
                        }
                    }
                }
            }
        }
    }

    return moves;
}

unsigned int Board::getCurrentPlayer()
{
    return static_cast<unsigned int>(current_player);
}
