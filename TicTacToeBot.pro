TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    board.cpp \
    rollout.cpp \
    communicator.cpp \
    mcts.cpp \
    node.cpp

HEADERS += \
    board.h \
    rollout.h \
    communicator.h \
    strategy.h \
    mcts.h \
    node.h

