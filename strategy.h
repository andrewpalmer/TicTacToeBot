#ifndef STRATEGY_H
#define STRATEGY_H

#include <utility>

class Strategy
{
public:
    Strategy(){}

    // Called by the communicator when a move is requested
    virtual void playMove(const unsigned int &timebank) = 0;

    // Called by the communicator when the opponent has made a move
    virtual void updateState(std::pair<unsigned int, unsigned int> move) = 0;

    virtual void playerID(unsigned int &id) = 0;
    virtual void timebankMax(unsigned int &t) = 0;
    virtual void timePerTurn(unsigned int &t) = 0;

    virtual void exit() = 0;
};

#endif // STRATEGY_H
