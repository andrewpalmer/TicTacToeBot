#include "mcts.h"

MCTS::MCTS()
    : player_ID(0), timebank_max(10000), time_per_turn(500), move_flag(false), timebank_remaining(10000), exit_flag(false), opponent_move_flag(false)
{
    root = std::shared_ptr<Node>(new Node);
}

void MCTS::playMove(const unsigned int &timebank)
{
    timebank_remaining = timebank;
    move_flag = true;
}

void MCTS::updateState(std::pair<unsigned int, unsigned int> move)
{
    opponent_move = move;
    opponent_move_flag = true;
}

void MCTS::playerID(unsigned int &id)
{
    player_ID = id;
    std::cerr << "Updated player ID in MCTS to " << player_ID << std::endl;
}

void MCTS::timebankMax(unsigned int &t)
{
    timebank_max = t;
    std::cerr << "Updated max timebank in MCTS to " << timebank_max << std::endl;
}

void MCTS::timePerTurn(unsigned int &t)
{
    time_per_turn = t;
    std::cerr << "Updated time per turn in MCTS to " << time_per_turn << std::endl;
}

void MCTS::exit()
{
    exit_flag = true;
}

void MCTS::oneSearch()
{
    // Go down the tree until a leaf node is reached
    std::shared_ptr<Node> currentNode = root;
    unsigned char winner;

//    std::cerr << "Starting search" << std::endl;


    while (!currentNode->checkLeafNode())
    {
//        std::cerr << "Not a leaf node, getting child" << std::endl;

        // While we're not at a leaf node, get the next node
        parents.push_back(currentNode);
        currentNode = currentNode->getBestNodeUCT(player_ID);

        currentNode->updateNumVisits(); // Increment the number of times that node has been visited
        // Note that this will automatically spawn the children if it has been visited enough
    }

    // Now we're at a leaf node
    // Check whether it is a terminal node
    if (currentNode->checkTerminalNode())
    {
        // Then the winner is given by guaranteedWinner
        winner = currentNode->checkGuaranteedWinner();
    }
    else
    {
//        std::cerr << "Performing a rollout" << std::endl;

        // Otherwise do a rollout
        rollout.copyFromBoard(currentNode->getCurrentState());
        winner = rollout.rollout();
    }

    unsigned int scoreIncrease = 0;
    if (winner == 0)
    {
        scoreIncrease = Node::drawScore;
    }
    else if (winner == player_ID)
    {
        scoreIncrease = Node::winScore;
    }

    // Now propagate back up the tree
    while (parents.size() != 0)
    {
        currentNode->updateScore(scoreIncrease);
        currentNode = parents.back();
        parents.pop_back();
    }
}

void MCTS::searchFor(unsigned int t)
{
    std::chrono::system_clock::time_point end = std::chrono::system_clock::now() + std::chrono::milliseconds(t);

    unsigned int count = 0;
    while (std::chrono::system_clock::now() < end)
    {
        oneSearch();
        count++;
    }

    std::cerr << "Searched for " << t << " milliseconds and performed " << count << " episodes" << std::endl;
}

void MCTS::sendMove(std::pair<unsigned int, unsigned int> &move)
{
    std::cout << "place_move " << move.first << " " << move.second << std::endl;
}

unsigned int MCTS::calcSearchDuration()
{
    return timebank_remaining / 2 + time_per_turn / 2 - 10;
}

void MCTS::run()
{
    std::pair<unsigned int, unsigned int> move;
    while (player_ID == 0 && !exit_flag)
    {
        // Do nothing until we know the player_ID
    }

    if (player_ID == 1)
    {
        std::cerr << "Performing the first move" << std::endl;

        // Update state for playing move (4,4)
        move = std::pair<unsigned int, unsigned int>(4,4);
        root = root->getChildNode(move);

        // Search for ~490ms
        searchFor(time_per_turn / 2 - 10);

        // Send move 4,4 to server
        sendMove(move);
    }

    while (!exit_flag)
    {
        if (opponent_move_flag)
        {
            std::cerr << "Opponent performed a move" << std::endl;
            root = root->getChildNode(opponent_move);
            opponent_move_flag = false;
        }
        else if (move_flag)
        {
            std::cerr << "Move requested" << std::endl;

            // Calculate how long to search for, then search for that long
            searchFor(calcSearchDuration());

            // Send the best move
            move = root->getBestMoveToPlay();
            sendMove(move);
            move_flag = false;
        }
        else
        {
            // Run an episode
            oneSearch();
        }
    }
}

void MCTS::best()
{
    root->getBestMoveToPlay();
}
